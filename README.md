![Screenshot preview of the theme "Whimsical" by glenthemes](https://64.media.tumblr.com/502c250d7b70f8d53c9013590cee3ea6/0d1b54772c9e1d67-14/s1280x1920/f48f7907e62497eb20b7444fc427e906e1d8ad7d.png)

**Theme no.:** 04  
**Theme name:** Whimsical  
**Theme type:** Free / Tumblr use  
**Description:** Originally known as Timeless a *very* long time ago, this theme was reincarnated as Whimsical in 2018, a fun and summery multi-columned theme.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-08-??](https://64.media.tumblr.com/9b48cd37b10b4c4cae2bf829dd568022/tumblr_nrmwzjerYU1ubolzro3_r1_540.gif)  
**Rework date [v1]:** [2018-09-27](https://64.media.tumblr.com/f3dce28f9b8708882522b0ad21bf151e/tumblr_pfqdzqMAhz1ubolzro2_r1_1280.png)  
**Rework date [v2]:** 2022-01-28

**Post:** [glenthemes.tumblr.com/post/178523953004](https://glenthemes.tumblr.com/post/178523953004)  
**Preview:** [glenthpvs.tumblr.com/whimsical](https://glenthpvs.tumblr.com/whimsical)  
**Download:** [pastebin.com/dcT2RBTe](https://pastebin.com/dcT2RBTe)  
**Guide:** [docs.google.com/presentation/d/1FVZWJQes4r_UqANvDcTa88uSCWTpFVyZMCTRPO4VMYU/edit?usp=sharing](https://docs.google.com/presentation/d/1FVZWJQes4r_UqANvDcTa88uSCWTpFVyZMCTRPO4VMYU/edit?usp=sharing)  
**Credits:** [glencredits.tumblr.com/whimsical](https://glencredits.tumblr.com/whimsical)
