// what is the secret to a fulfilling life?
// good rice.

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;

window.root = getComputedStyle(document.documentElement);

document.addEventListener("DOMContentLoaded",() => {
    var bob = document.getElementsByTagName("html")[0];

    if(customize_page){
        bob.setAttribute("customize-page","true");
    } else if(on_main){
        bob.setAttribute("customize-page","false");
    }
});
    
// audio post play button - flaticon.com/free-icon/play-button_152770
var playb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Layer_1' x='0px' y='0px' viewBox='0 0 330 330' style='enable-background:new 0 0 330 330;' xml:space='preserve'> <path id='XMLID_497_' d='M292.95,152.281L52.95,2.28c-4.625-2.891-10.453-3.043-15.222-0.4C32.959,4.524,30,9.547,30,15v300 c0,5.453,2.959,10.476,7.728,13.12c2.266,1.256,4.77,1.88,7.272,1.88c2.763,0,5.522-0.763,7.95-2.28l240-149.999 c4.386-2.741,7.05-7.548,7.05-12.72C300,159.829,297.336,155.022,292.95,152.281z M60,287.936V42.064l196.698,122.937L60,287.936z'/> </svg>";

document.documentElement.style.setProperty('--audioplay','url("' + playb + '")');

// audio post pause button - flaticon.com/free-icon/pause_747384
var pauseb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M154,0H70C42.43,0,20,22.43,20,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C204,22.43,181.57,0,154,0z M164,462c0,5.514-4.486,10-10,10H70c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> <g> <g> <path d='M442,0h-84c-27.57,0-50,22.43-50,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C492,22.43,469.57,0,442,0z M452,462c0,5.514-4.486,10-10,10h-84c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--audiopause','url("' + pauseb + '")');

// audio post 'install audio' button
// feathericons
var cdrii = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg>";

document.documentElement.style.setProperty('--install','url("' + cdrii + '")');

// external link icon
// feathericons
var schtd = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-external-link'><path d='M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6'/><polyline points='15 3 21 3 21 9'/><line x1='10' y1='14' x2='21' y2='3'/></svg>";

document.documentElement.style.setProperty('--ext','url("' + schtd + '")');

// glen svg
var cjaj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='150.000000pt' height='150.000000pt' viewBox='0 0 150.000000 150.000000' preserveAspectRatio='xMidYMid meet'> <g transform='translate(0.000000,150.000000) scale(0.100000,-0.100000)' stroke='none'> <path d='M476 1267 c-249 -98 -363 -148 -373 -163 -11 -17 -14 -92 -14 -389 0 -363 1 -369 22 -391 20 -22 537 -234 570 -234 7 0 137 48 287 107 188 73 277 113 284 127 7 12 11 151 12 383 l1 364 105 42 c114 46 132 65 77 85 -61 22 -20 36 -600 -193 l-169 -66 -199 77 c-109 43 -198 79 -196 80 8 9 565 224 578 224 9 0 79 -25 155 -55 77 -30 149 -55 162 -55 12 0 39 7 59 15 32 13 35 17 22 30 -19 19 -372 155 -401 155 -13 -1 -184 -65 -382 -143z m-16 -336 c113 -45 214 -81 225 -81 11 0 123 41 248 90 126 50 230 90 233 90 2 0 3 -149 2 -331 l-3 -331 -200 -79 c-110 -43 -208 -82 -217 -85 -17 -5 -18 7 -18 165 0 208 4 202 -150 255 l-106 37 -47 -18 c-26 -10 -46 -22 -44 -28 2 -5 53 -28 113 -50 60 -22 115 -44 122 -48 18 -12 18 -320 0 -315 -7 2 -109 41 -225 88 l-213 85 0 332 0 332 38 -13 c20 -8 129 -50 242 -95z'/> </g> </svg>";

document.documentElement.style.setProperty('--glenSVG','url("' + cjaj + '")');
// =>

$(document).ready(function(){
    // check jquery version
    var jqver = jQuery.fn.jquery;
    jqver = jqver.replaceAll(".","");
    
    $(".tumblr_preview_marker___").remove();
    
    /*-------- TOOLTIPS --------*/
    $("a[title]:not([title=''])").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:0,
        tip_fade_speed:269,
        attribute:"title"
    });
    
    /*-------- POSTS MASONRY --------*/
    if($("html").attr("index-page") == "true" && $("html").attr("post-cols") !== "1"){
        griddery($(".posts"), "--Post-Columns");
    }
    
    /*------- NPF VIDEO STUFF -------*/
    $(".photo-origin").each(function(){
        if($(this).find("[data-npf*='video']").length){
            $(this).addClass("npf-video")
        }
    })
    
    $("video[autoplay='autoplay']").each(function(){
        $(this).removeAttr("autoplay")
    });
    
    /*----------- REMOVE ALL WHITESPACES -----------*/
    $(".postinner p").each(function(){
        if(!$(this).prev().length){
            if($(this).parent().is(".postinner")){

                $(this).css("margin-top",0)
            }
        }
        
        if(!$(this).next().length){
            // target last <p>
            // if it's empty, remove
            if($.trim($(this).html()) == ""){
                $(this).remove();
            }
        }
    })
    
    $(".postinner p, .postinner blockquote, .postinner ol, .postinner ul").each(function(){
        if(!$(this).next().length){
            // target last <p>
            // if no next sibling, negate bottom padding
            $(this).css("margin-bottom",0)
        }
        
        if($(this).next().is(".tagsdiv")){
            $(this).css("margin-bottom",0)
        }
    })
    
    // remove empty captions
    $(".caption").each(function(){
        if($.trim($(this).text()) == ""){
            $(this).remove()
        }
    })
    
    // wrap all text nodes with span
    $("[original-post] .postinner, [reblogged-post] .reblog-comment, .quote-source, .permaleft a, .dyn").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span/>")
    })
    
    if(customize_page){
        $(".reblog-comment > p[last-comment]:first-child").each(function(){
            $(this).css("margin-top",0)
        })
    }
    
    $(".posts").each(function(){
        $(this).find("p, h1, h2, h3, h4, h5, h6, blockquote, ol, ul, pre").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })
    
    $(".sb-title").each(function(){
        $(this).html($.trim($(this).html()));
    })
    
    $(".dyn").each(function(){
        $(this).find("p:first").each(function(){
            if(!$(this).prev().length){
                $(this).css("margin-top",0)
            }
        })
        
        $(this).find("p:last").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })
    
    /*----------- REBLOG-HEAD -----------*/
    $(".reblog-url").each(function(){
        var uz = $.trim($(this).text());
        if(uz.indexOf("-deac") > 0){
            var rogner = uz.substring(0,uz.lastIndexOf("-"));
            $(this).find("a").attr("href","//" + rogner + ".tumblr.com");
            $(this).find("a").text(rogner);
            $(this).append("<span class='deac'>(deactivated)</span>")
        }
    })
    
    /*-------- AUDIO BULLSH*T --------*/
    var mtn = Date.now();
    var fvckme = setInterval(function(){
        if(Date.now() - mtn > 1000){
            clearInterval(fvckme);
            $(".audiowrap").each(function(){
                $(this).prepend("<audio src='" + $(this).attr("audio-src") + "'>");
            });
            
            $(".inari").each(function(){
                var m_m = $(this).parents(".audiowrap").attr("audio-src");
                $(this).attr("href",m_m);
            })
        } else {
            $(".tumblr_audio_player").each(function(){
                if($(this).is("[src]")){
                    var audsrc = $(this).attr("src");
                    audsrc = audsrc.split("audio_file=").pop();
                    audsrc = decodeURIComponent(audsrc);
                    audsrc = audsrc.split("&")[0];
                    $(this).parents(".audiowrap").attr("audio-src",audsrc)
                }
            })
        }
    },0);
    
    $(".albumwrap").click(function(){
        
        var emp = $(this).parents(".audiowrap").find("audio")[0];
        
        if(emp.paused){
            emp.play();
            $(".overplay",this).addClass("ov-z");
            $(".overpause",this).addClass("ov-y");
        } else {
            emp.pause();
            $(".overplay",this).removeClass("ov-z");
            $(".overpause",this).removeClass("ov-y");
        }
        
        var that = this
        
        emp.onended = function(){
            $(".overplay",that).removeClass("ov-z");
            $(".overpause",that).removeClass("ov-y");
        }
    })
    
    // minimal soundcloud player ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â© shythemes.tumblr
    var color = getComputedStyle(document.documentElement)
               .getPropertyValue("--Body-Text-Color");
    
    $('iframe[src*="soundcloud.com"]').each(function(){
        $(this).one("load",function(){
            soundfvk()
        });
    });
    
    function soundfvk(){
       $('iframe[src*="soundcloud.com"]').each(function(){
           $(this).attr({ src: $(this).attr('src').split('&')[0] + '&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=true&amp;origin=tumblr&amp;color=' + color.split('#')[1], height: 116, width: '100%' });
       });
    }
    
    $(".soundcloud_audio_player").each(function(){
        $(this).wrap("<div class='audio-soundcloud'>")
    })
    
    /*-------- QUOTE SOURCE BS --------*/
    $(".quote-source").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span>");
        $(this).find("a.tumblr_blog").remove();
        
        $(this).find("span").each(function(){
            if($.trim($(this).text()) == "(via"){
                $(this).remove();
            }
            
            if($.trim($(this).text()) == ")"){
                $(this).remove();
            }
        })
        
        $(this).html(
            $(this).html().replace("(via","")
        )
        
        $(this).find("span:first").each(function(){
            if(!$(this).next().length){
                $(this).remove()
            }
        })
        
        $(this).find("p").each(function(){
            if($.trim($(this).text()) == ""){
                $(this).remove();
            }
            
            if($(this).children("br").length){
                if(!$(this).children().first().siblings().length){
                    $(this).remove()
                }
            }
            
            $(this).html($.trim($(this).html()));
            
            if($(this).text() == ")"){
                $(this).remove()
            }
        })
        
        $(this).find("p:last").each(function(){
            if($(this).find("a[href*='tumblr.com/post']").length){
                $(this).remove()
            }
        })
        
        $(this).find("p:last").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })
    
    $("[mdash] + p").each(function(){
        if(!$(this).next().length){
            if($.trim($(this).text()) !== ""){
                var sto = " " + $(this).html();
                $(this).prev().append(sto)
                $(this).remove();
            }
        }
    })
    
    /*-------- ASK/ANSWER POSTS --------*/
    $(".question_text").each(function(){
        if(!$(this).children().first().is("p")){
            $(this).wrapInner("<p></p>")
        }
    })
    
    /*-------- CHAT POSTS --------*/
    $(".npf_chat").each(function(){
        $(this).find("b").each(function(){
            var cb = $(this).html();
            $(this).before("<div class='chat_label'>" + cb + "</div>");
            $(this).remove()
        })
        
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<div class='chat_content'>");
        
        $(this).wrap("<div class='chat_row'>");
        $(this).children().unwrap()
    })
    
    $(".chat_row").each(function(){
        $(this).not(".chat_row + .chat_row").each(function(){
            if(jqver < "180"){
                $(this).nextUntil(":not(.chat_row)").andSelf().wrapAll('<div class="chatwrap">');
            } else {
                $(this).nextUntil(":not(.chat_row)").addBack().wrapAll('<div class="chatwrap">');
            }
        });
    })

    $(".chat_row .chat_content:first").each(function(){
        if($(this).nextAll(".chat_content").length){
			$(this).nextAll().appendTo($(this));
			
		}
    })
	
	$(".chat_row > .chat_content").each(function(){
		var that = this;
		$(this).find(".chat_content").each(function(){
			var xvcy = $(this).html();
			$(this).remove();
			$(that).append(xvcy);
		})
	})
    
    /*---- MAKE SURE <p> IS FIRST CHILD OF RB ----*/
    $(".reblog-head").each(function(){
        if(!$(this).next(".reblog-comment").length){
            $(this).nextUntil(".tagsdiv").wrapAll("<div class='reblog-comment'>")
        }
    })
    
    $(".reblog-comment").each(function(){
        if($(this).children().first().is("div")){
            $(this).prepend("<p></p>")
        }
    })
    
    
    
    /*-------- CLICKTAGS --------*/
    var tags_ms = parseInt(getComputedStyle(document.documentElement)
                   .getPropertyValue("--Tags-Fade-Speed-MS"));
    
    
    $(".clicktags").click(function(){
        var that = this;
        var tagsdiv = $(this).parents(".permadiv").prev(".postinner").find(".tagsdiv");
        
        if(!$(this).hasClass("clique")){
            $(this).addClass("clique");
            tagsdiv.slideDown(tags_ms);
            setTimeout(function(){
                tagsdiv.addClass("tagsfade");
            },tags_ms);
        } else {
            tagsdiv.removeClass("tagsfade");
            setTimeout(function(){
                tagsdiv.slideUp(tags_ms);
                $(that).removeClass("clique");
            },tags_ms)
        }
    })
    
    /*----------- POST NOTES -----------*/
    // nothing yet
    
    // remove tumblr redirects script by magnusthemes@tumblr
    // part 1/2
    $('a[href*="t.umblr.com/redirect"]').each(function(){
      var originalURL = $(this).attr("href").split("?z=")[1].split("&t=")[0];
      var replaceURL = decodeURIComponent(originalURL);
      $(this).attr("href", replaceURL);
    });
    
    // part 2/2
    function noHrefLi(){
        var linkSet = document.querySelectorAll('a[href*="href.li/?"]');
        Array.prototype.forEach.call(linkSet,function(el,i){
            var theLink = linkSet[i].getAttribute('href').split("href.li/?")[1];
            linkSet[i].setAttribute("href",theLink);
        });
    }
    noHrefLi();
    
    // make iframe heights look more 'normal'
	$(".embed_iframe").each(function(){
        if($(this).parent().is(".tmblr-embed")){
            var wut = $(this).width();
            
            var wrath = $(this).attr("width");
            var rat_w = wrath / wut;
            
            var hrath = $(this).attr("height");
            var rat_h = hrath / rat_w;
            
            $(this).height(rat_h)
        }
    })
    
    /*--- fvck tvmblr ---*/
    var imgs = document.querySelectorAll("img");
    Array.prototype.forEach.call(imgs, function(invis){	
      if(invis.src.indexOf("assets.tumblr.com/images/x.gif") > -1){
        invis.setAttribute("src","https://cdn.glitch.com/bdf00c8f-434a-46d9-a514-ec8332ec176a/1x1.png");
      }
    });
    
    /*----- OTHER -----*/
    
    $(".chat_content").each(function(){
        if($.trim($(this).text()).indexOf("{block:") > -1){
            var notgod = $(this).html();
            notgod.replaceAll("{","&lcub;").replaceAll("}","&rcub;");
            $(this).before("<code>" + notgod + "</code>");
            $(this).remove()
        }
    })
    
    var srtn = $.trim(root.getPropertyValue("--Shorten-LongPosts"));
    var lpexp = $.trim(root.getPropertyValue("--LongPosts-Action"));
    var lpanitym = parseInt($.trim(root.getPropertyValue("--LongPosts-Expand-Speed")));
    var exptxt = $.trim(root.getPropertyValue("--LongPosts-Expand-Text")).slice(1).slice(0,-1);
    
    if(srtn == "yes" && $("html").attr("index-page") == "true"){
        
        var winh = $(window).height();
        
        $(".postinner").each(function(){
            var stoer = $(this).height();
            
            if($(this).height() > winh){
                var postssh = $(this).parents(".posts").attr("post-url");
                $(this).height(winh * 0.420);
                $(this).addClass("envee");
                $(this).prepend("<div class='ensheet'></div>");
                
                if(lpexp == "expand"){
                    $(this).find(".ensheet").after("<div class='lynxy nyh'><a>" + exptxt + "</a></div>");
                    $(this).parents(".posts").find(".clicktags").attr("title",'click "' + exptxt + '" to see the tags!');
                    
                    $(".lynxy").click(function(){
                        var that = this;
                        var lynxpar = $(this).parents(".postinner");
                        $(this).add($(this).prev(".ensheet")).fadeOut(lpanitym);
                        
                        lynxpar.parents(".posts").find(".clicktags").removeAttr("title");
                        
                        setTimeout(function(){
                            lynxpar.css("height",stoer);
                            lynxpar.removeClass("envee");
                            
                            setTimeout(function(){
                                lynxpar.css("height","")
                            },lpanitym);
                        },lpanitym);
                    })
                } else if(lpexp == "postlink"){
                    $(this).find(".ensheet").after("<div class='lynxy'><a href=\'" + postssh + "\'>keep reading</a></div>");
                }
                
            }
        })
    
    }// end if YES shorten posts
    
    // read more button
    $(".tmblr-truncated-link.read_more").each(function(){
        $(this).parent("p").addClass("p-more");
    })
    
    $(".customlinks").each(function(){
        if($.trim($(this).html()) == ""){
            $(this).remove();
        }
    })
    
    // attempt to do smth with the uneven heights caused by griddery
    $(".col-column").each(function(){
        var poth = 0;
        $(this).find(".col-item").each(function(){
            poth += $(this).outerHeight();
        });
        
        $(this).attr("genheight",poth)
    })
    
    var winhh = $(window).height();
    
    $("[genheight] + [genheight]").each(function(){
        var col1h = Number($(this).prev().attr("genheight"));
        var col2h = Number($(this).attr("genheight"));
        var coldiff = col1h - col2h;
        
        // if first column has more cells than 2nd column,
        // AND first column is taller than the 2nd
        if($(this).prev().children(".col-item").length > $(this).children(".col-item").length && coldiff > winhh * 0.69){
            $(this).prev().find(".col-item:last").appendTo($(this))
        }
    })
    
    /*--- MUSIC PLAYER ---*/
    if($(".m-usicplayer").length){
        var bbn_ = $(".m-usicplayer").find("audio");
        
        var bbs = bbn_.attr("src");
        bbn_.attr("src",bbs.replace("www.dropbox.com","dl.dropbox.com").replace("?dl=0",""));
        
        var bbn = $(".m-usicplayer").find("audio")[0];
        
        var vvn = $.trim($(".m-usicplayer").attr("volume"));
        vvn = parseInt(vvn) / 100;
        
        bbn.volume = vvn;
        
        if(bbn_.is("[autoplay]")){
            $(".pl-c").addClass("bhh");
            $(".pa-c").addClass("bhz");
        }
        
        $(".cciuq").click(function(){
            if(bbn.paused){
                bbn.play();
                $(".pl-c").addClass("bhh");
                $(".pa-c").addClass("bhz");
            } else {
                bbn.pause();
                $(".pl-c").removeClass("bhh");
                $(".pa-c").removeClass("bhz");
            }
        })
        
        bbn_.bind("ended", function(){
            $(".pl-c").removeClass("bhh");
            $(".pa-c").removeClass("bhz");
        });
        
        bbn_.bind("pause", function(){
            $(".pl-c").removeClass("bhh");
            $(".pa-c").removeClass("bhz");
        });
        
        bbn_.bind("play", function(){
            $(".pl-c").addClass("bhh");
            $(".pa-c").addClass("bhz");
        });
    }
    
    /*--- CHAD ---*/
    if(!$(".navlinks a[href*='glenthemes.tumblr.com']").length && !$(".healsdeez").length){
        $("body").append('<a class="healsdeez" href="//glenthemes.tumblr.com" title="&quot;whimsical&quot; theme by glenthemes &#x266A;(^&#x2207;^*)"><img src="https://orig00.deviantart.net/90dc/f/2009/019/7/7/whale_avatar_by_grasshoneyz.gif"></a>');
    }
    
    /*--- BIG BEAN ---*/
    if(customize_page){
        setTimeout(function(){
            $("body").append("<div class='msgbby'>Hello! Thank you for using Whimsical! Please read <a href=\'//docs.google.com/presentation/d/1FVZWJQes4r_UqANvDcTa88uSCWTpFVyZMCTRPO4VMYU/edit?usp=sharing\'>the guide</a> to get started.</div>");
        },800);
    }    
    
});//end jquery / end ready
